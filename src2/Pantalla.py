#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *


class Ventana:
    def __init__(self):
        #sección de inicialización
        self.ANCHO = 500
        self.ALTO = 500
        self.escala = 50
        self.salir = False
        self.reloj = pygame.time.Clock()
        pygame.init()
        self.dimension = [self.ANCHO, self.ALTO]
        self.pantalla = pygame.display.set_mode(self.dimension)
        pygame.display.set_caption("Juego nro1 Pygame")

    def actualizar(self):
        pygame.display.update()


class Pantalla(Ventana):
    def __init__(self):
        #sección de inicialización
        Ventana.__init__(self)

    def iniciar(self):
        while self.salir is not True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salir = True
            if self.salir is not True:
                self.actualizar()
        pygame.quit()



