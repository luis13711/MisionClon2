# -*- coding: utf-8 -*-


class ColisionVentana:
    def __init__(self, ventana, personaje):
        self.ventana = ventana
        self.personaje = personaje

    def derecha(self):
        tempx = self.personaje.rect.x + self.personaje.rect.width
        if tempx > self.ventana.width:
            res = self.ventana.width - self.personaje.rect.width
            self.personaje.rect.x = res

    def izquierda(self):
        if self.personaje.rect.x < self.ventana.x:
            self.personaje.rect.x = self.ventana.x

    def arriba(self):
        if self.personaje.rect.y < self.ventana.y:
            self.personaje.rect.y = self.ventana.y

    def abajo(self):
        tempy = self.personaje.rect.y + self.personaje.rect.height
        if tempy > self.ventana.height:
            res = self.ventana.height - self.personaje.rect.height
            self.personaje.rect.y = res