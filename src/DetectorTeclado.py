#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *


class DetectorTeclado:
    def __init__(self, personaje):
        self.jugador = personaje

    def mover(self, teclado):
        if teclado[pygame.K_RIGHT]:
            self.jugador.derecha()
        if teclado[pygame.K_LEFT]:
            self.jugador.izquierda()
        if teclado[pygame.K_UP]:
            self.jugador.arriba()
        if teclado[pygame.K_DOWN]:
            self.jugador.abajo()

    def dibujar(self, pantalla):
        self.jugador.dibujar(pantalla)
