# -*- coding: utf-8 -*-
from pygame.locals import *
from Personaje import *
from Punto import *
from Mapa import *
from Enemigo import *
from DetectorTeclado import *
from Mundo import *


class PanelJuego:
    def __init__(self, mapa, dimension, Pantalla):
        #sección de componentes del juego
        self.mapa = mapa
        self.dimension = dimension
        #escala de juego 50
        #tamaniom = pygame.Rect(0, 0, 2000, 500)
        self.Pantalla = Pantalla
        self.mundo = Mundo(self.mapa, self.dimension, self.Pantalla.pantalla)
        #id del panel
        self.id = 2

    def getId(self):
        return self.id

    def dibujar(self):
        self.mundo.actualiza(self.Pantalla.pantalla)

    def establecerPunto(self, punto):
        self.punto = punto