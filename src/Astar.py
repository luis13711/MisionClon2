# -*- coding: utf-8 -*-
from Punto import *


class Busqueda:
    #// 0 es vacio
     #// 1 jugador
    #//2 es comida
    #//3 es obstaculos
    #//4 son enemigos
    def __init__(self, personaje):
        #guardar el mapa para el conteo de frutas
        self.mapa = personaje.mapa
        self.matriz = personaje.mapa.matriz
        self.tamaniox = personaje.mapa.columnas()
        self.tamanioy = personaje.mapa.filas()
        self.personaje = personaje
        self.listacerrada = []
        self.iniciar = True

    def menorpeso(self, B, adyacentes):
        fp = 10000
        mejor = None
        for temporal in adyacentes:
            F = 0
            G1 = 10
            G2 = 14
            H = self.distancias(temporal, B)
            if temporal.x < B.x and temporal.y < B.y:
                F = G2 + H
            elif temporal.x < B.x:
                F = G1 + H
            elif temporal.y < B.y:
                F = G1 + H
            elif temporal.x > B.x and temporal.y > B.y:
                F = G2 + H
            elif temporal.x > B.x:
                F = G1 + H
            elif temporal.y > B.y:
                F = G1 + H
            elif temporal.x < B.x and temporal.y > B.y:
                F = G2 + H
            elif temporal.x > B.x and temporal.y < B.y:
                F = G2 + H
            if F < fp:
                fp = F
                mejor = temporal
        return mejor

    def addlistacerrada(self, B):
        self.listacerrada.append(B)

    def listacerrada(self):
        return self.listacerrada

    def adyacentes(self, A):
        i = A.y
        j = A.x
        self.listacerrada.append(A)
        listaabierta = []
        if i - 1 > -1:
            matx = int(i - 1)
            maty = int(j)
            if (self.matriz[matx][maty] < 3):
                #listaabierta.append(Punto(j, i - 1))
                listaabierta = self.agregar_lista(j, i - 1, listaabierta)

            if j - 1 > -1:
                matx = int(i - 1)
                maty = int(j - 1)
                if (self.matriz[matx][maty]) < 3:
                    cx = j - 1
                    cy = i - 1
                    listaabierta = self.agregar_lista(cx, cy, listaabierta)
                    #listaabierta.append(Punto(j - 1, i - 1))

            if j + 1 < self.tamaniox:
                matx = int(i - 1)
                maty = int(j + 1)
                if (self.matriz[matx][maty] < 3):
                    #listaabierta.append(Punto(j + 1, i - 1))
                    cx = j + 1
                    cy = i - 1
                    listaabierta = self.agregar_lista(cx, cy, listaabierta)

        if j - 1 > -1:
            matx = int(i)
            maty = int(j - 1)
            if (self.matriz[matx][maty] < 3):
                #listaabierta.append(Punto(j - 1, i))
                listaabierta = self.agregar_lista(j - 1, i, listaabierta)

            if i + 1 < self.tamanioy:
                matx = int(i + 1)
                maty = int(j - 1)
                if (self.matriz[matx][maty] < 3):
                    #listaabierta.append(Punto(j - 1, i + 1))
                    cx = j - 1
                    cy = i + 1
                    listaabierta = self.agregar_lista(cx, cy, listaabierta)

        if i + 1 < self.tamanioy:
            matx = int(i + 1)
            maty = int(j)
            if (self.matriz[matx][maty] < 3):
                #listaabierta.append(Punto(j, i + 1))
                listaabierta = self.agregar_lista(j, i + 1, listaabierta)

        if j + 1 < self.tamaniox and i + 1 < self.tamanioy:
            matx = int(i + 1)
            maty = int(j + 1)
            if (self.matriz[matx][maty] < 3):
                #listaabierta.append(Punto(j + 1, i + 1))
                listaabierta = self.agregar_lista(j + 1, i + 1, listaabierta)

        if j + 1 < self.tamaniox:
            matx = int(i)
            maty = int(j + 1)
            if (self.matriz[matx][maty] < 3):
                #listaabierta.append(Punto(j + 1, i))
                listaabierta = self.agregar_lista(j + 1, i, listaabierta)

        #if not len(self.listacerrada) == 0:
            #for temp in self.listacerrada:
                #listaabierta.remove(temp)
        return listaabierta

    def repetido(self, punto1):
        for li in self.listacerrada:
            if punto1.x == li.x and punto1.y == li.y:
                return True
        return False

    def agregar_lista(self, x, y, listaabierta):
        punto1 = Punto(x, y)
        if not self.repetido(punto1):
            listaabierta.append(punto1)
        return listaabierta

    def cercano(self, A):
        circulos = []
        i = 0
        for filas in self.matriz:
            j = 0
            for columna in filas:
                if columna == 2:
                    circulos.append(Punto(j, i))
                    #j es x, i es y
                j += 1
            i += 1
        dp = 1000
        cercano = None
        for temporal in circulos:
            if self.distancias(A, temporal) < dp:
                cercano = temporal
                dp = self.distancias(A, cercano)
        return cercano

    def distancias(self, A, B):
#//        double primero=(A.x==B.x)?0:((A.x<B.x)?(B.x-A.x):(A.x-B.x));
#//        double segundo=(A.y==B.y)?0:((A.y<B.y)?(B.y-A.y):(A.y-B.y));
        primero = 0
        if A.x == B.x:
            primero = 0
        elif A.x < B.x:
            primero = B.x - A.x
        elif A.x > B.x:
            primero = A.x - B.x
        segundo = 0
        if A.y == B.y:
            segundo = 0
        elif A.y < B.y:
            segundo = B.y - A.y
        elif A.y > B.y:
            segundo = A.y - B.y
        return primero + segundo

    def astar(self, inicial):
        A = inicial
        B = self.cercano(A)
        self.listacerrada = []
        #while da error al acabarse la fruta crear metodo que verifique
        #Necesito saber cuantas frutas quedan siempre
        if self.mapa.getContador() != 0:
            while A.x != B.x or A.y != B.y:
                A = self.menorpeso(B, self.adyacentes(A))
            self.listacerrada.append(B)
            self.remover(inicial)
            return self.listacerrada

    def remover(self, inicial):
        for li in self.listacerrada:
            if inicial.x == li.x and inicial.y == li.y:
                self.listacerrada.remove(li)

    def mover(self):
        personaje = self.personaje
        escala = 50
        if self.iniciar:
            x = personaje.rect.x / escala
            y = personaje.rect.y / escala
            A = Punto(x, y)
            print(x, y)
            self.listacerrada = self.astar(A)
            self.iniciar = False
        if self.mapa.getContador() != 0:
            if not len(self.listacerrada) == 0:
                temp = self.listacerrada[0]
                self.listacerrada.remove(temp)
                personaje.rect.x = temp.x * escala
                personaje.rect.y = temp.y * escala
                print(temp.x, temp.y)
                self.iniciar = self.fue_eliminado(personaje.mapa.matriz)
            else:
                self.iniciar = True

    def fue_eliminado(self, matriz):
        temp = None
        for li in self.listacerrada:
            temp = li
        if temp is not None:
            coord = temp
            elemento = matriz[coord.y][coord.x]
            if elemento == 0:
                return True
        return False



