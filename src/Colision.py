# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *


class Colisiones:
    def __init__(self, listacomida):
        self.mapa = listacomida.mapa
        self.sonidoRecoleccion = pygame.mixer.Sound('recoleccion.wav')

    def verificar(self, personaje):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 2:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    if personaje.rect.colliderect(rect):
                        #eliminamos la comida
                        self.mapa.remover(lfila, lcolum)
                        #damos puntos al personaje
                        personaje.puntuacion += 1
                        return True
                lcolum += 1
            lfila += 1
        return False

    def actualizar(self, personaje):
        if self.verificar(personaje):
            self.sonidoRecoleccion.play()
            self.mapa.comio()
