# -*- coding: utf-8 -*-
from pygame.locals import *
from Mapa import *
from Enemigo import *
from DetectorTeclado import *
from Mundo import *
from BotonDimension import BotonDimension
from Texto import *
import pygame
from MundoMapa import MundoMapa
from BotonImagen import BotonImagen
from Observador import *
from Guardador import *


class PanelMapa(Observador):
    def __init__(self, mapa, dimension, Pantalla):
        Observador.__init__(self)
        #sección de componentes del juego
        self.mapa = mapa
        self.dimension = dimension
        #escala de juego 50
        #tamaniom = pygame.Rect(0, 0, 2000, 500)
        self.Pantalla = Pantalla
        pantalla = self.Pantalla.pantalla
        self.mundo = MundoMapa(self.mapa, self.dimension, pantalla)
        #sección de redimension
        self.guardador = Guardador(self.Pantalla, self)
        #id del panel
        self.id = 3

        #Redimensión de eje X
        rect = pygame.Rect(0, 0, 50, 50)
        flechai = 'img/flechai.png'
        flechair = 'img/flechair.png'
        flechad = 'img/flechad.png'
        flechadr = 'img/flechadr.png'
        self.btnIXBotonDimension = self.btn(1, flechai, flechair, rect)
        rect = pygame.Rect(50, 0, 50, 50)
        self.btnDXBotonDimension = self.btn(2, flechad, flechadr, rect)
        #texto X
        rect = pygame.Rect(100, 0, 50, 50)
        self.textoX = Texto(rect, str(5))

        #Redimensión de eje Y
        rect = pygame.Rect(150, 0, 50, 50)
        self.btnIYBotonDimension = self.btn(3, flechai, flechair, rect)
        rect = pygame.Rect(200, 0, 50, 50)
        self.btnDYBotonDimension = self.btn(4, flechad, flechadr, rect)
        #texto Y
        rect = pygame.Rect(250, 0, 50, 50)
        self.textoY = Texto(rect, str(5))

        self.guardarRedimension()

        #boton de guardar
        rect = pygame.Rect(300, 0, 100, 50)
        self.btnGuardar = BtnTexto(rect, "Guardar")

        #fruta
        self.btnFruta = self.btnI('img/cereza.png', 0, 50)
        #heroe
        self.btnJugador = self.btnI('img/jugador.png', 0, 100)
        #casa
        self.btnCasa = self.btnI('img/edificio.png', 0, 150)
        #vilano
        self.btnVillano = self.btnI('img/villano.png', 0, 200)

    def guardarRedimension(self):
        #self.agregar(self.btnIXBotonDimension)
        #self.agregar(self.btnDXBotonDimension)
        #self.agregar(self.btnIYBotonDimension)
        #self.agregar(self.btnDYBotonDimension)
        self.btnIXBotonDimension.agregar(self)
        self.btnDXBotonDimension.agregar(self)
        self.btnIYBotonDimension.agregar(self)
        self.btnDYBotonDimension.agregar(self)

    def actualizar(self, id):
        if id is 1:
            print ('x menos')
            self.guardador.decrementarX()
        elif id is 2:
            print ('x más')
            self.guardador.aumentarX()
        elif id is 3:
            print ('y menos')
            self.guardador.decrementarY()
        elif id is 4:
            print ('y más')
            self.guardador.aumentarY()

    def btn(self, id, nombre, nombre2, rect):
        return BotonDimension(id, nombre, nombre2, rect)

    def btnI(self, nombre, x, y):
        return BotonImagen(nombre, x, y)

    def getId(self):
        return self.id

    def dibujar(self):
        #el algoritmo no llega al ultimo cuadrado
        if pygame.mouse.get_pressed()[0] is 1:
            punto = pygame.mouse.get_pos()
            rect = pygame.Rect(punto[0], punto[1], 5, 5)
            rectangulos = []
            matriz = self.Pantalla.mapa.matriz
            #dibujar mapa clic usuario
            lfila = 1
            for fila in range(len(matriz) + 1):
                lcolum = 1
                for columna in range(len(matriz[0]) + 1):
                    if not lfila is 0 and not lcolum is 0:
                        temp = pygame.Rect(lfila * 50, lcolum * 50, 50, 50)
                        rectangulos.append(temp)
                    lcolum += 1
                lfila += 1
            for temp in rectangulos:
                if rect.colliderect(temp):
                    print (temp.x, temp.y, temp.width, temp.height)
        pantalla = self.Pantalla.pantalla
        self.mundo.actualiza(pantalla)
        self.btnIXBotonDimension.dibujar(pantalla)
        self.btnDXBotonDimension.dibujar(pantalla)
        self.btnIYBotonDimension.dibujar(pantalla)
        self.btnDYBotonDimension.dibujar(pantalla)
        self.textoX.dibujar(pantalla)
        self.textoY.dibujar(pantalla)
        self.btnGuardar.dibujar(pantalla)
        self.btnFruta.dibujar(pantalla)
        self.btnJugador.dibujar(pantalla)
        self.btnCasa.dibujar(pantalla)
        self.btnVillano.dibujar(pantalla)

    def establecerPunto(self, punto):
        self.punto = punto


#class MPanelMapa:
    #def __init__(self):
