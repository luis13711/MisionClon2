# -*- coding: utf-8 -*-
from modelo import *
from Punto import *
from pygame.locals import *
import pygame
from Observable import Observable


class BotonDimension(Observable):
    def __init__(self, id, titulo, titulo2, rect):
        Observable.__init__(self)
        self.id = id
        self.rect = rect
        self.imagen = Imagen(titulo, rect, Punto(0, 0), False)
        self.titulo = titulo
        self.titulo2 = titulo2
        self.rect = rect

    def dibujar(self, pantalla):
        #Zona de evento
        if pygame.mouse.get_pressed()[0] is 1:
            #detecta posición de la flecha del mouse
            punto = pygame.mouse.get_pos()
            rectangulo = pygame.Rect(punto[0], punto[1], 5, 5)
            colision = rectangulo.colliderect(self.rect)
            if colision:
                #print('detecto')
                self.actualizar(self.id)
        #detecta posición de la flecha del mouse
        punto = pygame.mouse.get_pos()
        rectangulo = pygame.Rect(punto[0], punto[1], 5, 5)
        colision = rectangulo.colliderect(self.rect)
        if colision:
            self.imagen = Imagen(self.titulo2, self.rect, Punto(0, 0), False)
        else:
            self.imagen = Imagen(self.titulo, self.rect, Punto(0, 0), False)
        self.imagen.dibujo(pantalla, False)


#class BotonDimension(Imagen, Observador):
    #def __init__(self, titulo, rect):
        #self.rect = rect
        #Imagen.__init__(self, titulo, rect, Punto(0, 0), False)

    #def dibujar(self, pantalla):
        ##detecta posición de la flecha del mouse
        #punto = pygame.mouse.get_pos()
        #rectangulo = pygame.Rect(punto[0], punto[1], 5, 5)
        #colision = rectangulo.colliderect(self.rect)
        #self.dibujo(pantalla, False)