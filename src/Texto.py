# -*- coding: utf-8 -*-
from pygame.locals import *
import pygame

blanco = (255, 255, 255)
rojo = (255, 0, 0)
azul = (0, 0, 255)
negro = (0, 0, 0)


class Texto:
    def __init__(self, rect, titulo):
        self.rect = rect
        self.titulo = titulo

    def dibujar(self, pantalla):
        #punto del mouse
        punto = pygame.mouse.get_pos()
        rectangulo = pygame.Rect(punto[0], punto[1], 5, 5)
        colisiona = rectangulo.colliderect(self.rect)
        color = blanco
        texto = negro
        if colisiona:
            color = rojo
            texto = blanco
        #tamaño 30 de letra
        tam_letra = 30
        fuente = pygame.font.Font(None, tam_letra)
        mensaje = fuente.render(self.titulo, 1, texto)
        x = self.rect.x + self.rect.width / 3
        y = self.rect.y + self.rect.height / 3
        pygame.draw.rect(pantalla, color, self.rect, 0)
        pantalla.blit(mensaje, (x, y))


class BtnTexto:
    def __init__(self, rect, titulo):
        self.rect = rect
        self.titulo = titulo

    def dibujar(self, pantalla):
        #punto del mouse
        punto = pygame.mouse.get_pos()
        rectangulo = pygame.Rect(punto[0], punto[1], 5, 5)
        colisiona = rectangulo.colliderect(self.rect)
        color = blanco
        texto = negro
        if colisiona:
            color = rojo
            texto = blanco
        #tamaño 30 de letra
        tam_letra = 30
        fuente = pygame.font.Font(None, tam_letra)
        mensaje = fuente.render(self.titulo, 1, texto)
        x = self.rect.x + self.rect.width / 7
        y = self.rect.y + self.rect.height / 3
        pygame.draw.rect(pantalla, color, self.rect, 0)
        pantalla.blit(mensaje, (x, y))
