# -*- coding: utf-8 -*-
from Astar import Busqueda
from Personaje import Personaje


class Enemigo:
    def __init__(self, personaje):
        self.personaje = personaje
        self.busqueda = Busqueda(personaje)
        self.contador = 0

    def mover(self):
        self.contador += 1
        if self.contador == 10:
            self.busqueda.mover()
            self.personaje.colision()
            self.contador = 0

    def dibujar(self, pantalla):
        self.personaje.dibujar(pantalla)


class CEnemigo:
    def __init__(self, mapa):
        self.mapa = mapa
        self.lista = []
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 4:
                    personaje = Personaje(mapa, lcolum, lfila)
                    #personaje.cambiar_imagen2()
                    enemigo = Enemigo(personaje)
                    self.lista.append(enemigo)
                lcolum += 1
            lfila += 1

    def mover(self):
        for enemigo in self.lista:
            enemigo.mover()

    def dibujar(self, pantalla):
        for enemigo in self.lista:
            enemigo.dibujar(pantalla)
            enemigo.mover()