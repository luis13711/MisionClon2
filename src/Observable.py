# -*- coding: utf-8 -*-


class Observable:
    def __init__(self):
        self.objetos = []

    def agregar(self, objeto):
        self.objetos.append(objeto)

    def actualizar(self, id):
        for objeto in self.objetos:
            objeto.actualizar(id)