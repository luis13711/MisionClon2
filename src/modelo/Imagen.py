# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *


class Imagen:
    def __init__(self, nombre, rect, punto, transparente):
        #coordenadas de imagen
        self.rect = rect
        #imagen
        self.imagen = self.imagen(nombre, transparente)
        self.punto = punto

    def imagen(self, filename, transparent=False):
        try:
            image = pygame.image.load(filename)
            punto = (self.rect.width, self.rect.height)
            image = pygame.transform.scale(image, punto)
        except pygame.error as message:
            raise SystemExit(message)
        image = image.convert()
        if transparent:
            color = image.get_at((0, 0))
            image.set_colorkey(color, RLEACCEL)
        return image

    def cambiar_imagen(self, nombre, transparente):
        #imagen
        self.imagen = self.imagen(nombre, transparente)

    def dibujo(self, pantalla, recortar):
        if recortar is True:
            #imagen recortada
            x = self.rect.x + self.punto.x
            y = self.rect.y + self.punto.y
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, (0, 0), temp)
        else:
            #desplazamiento eje x
            x = self.rect.x - self.punto.x
            #desplazamiento eje y
            y = self.rect.y - self.punto.y
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, temp)

