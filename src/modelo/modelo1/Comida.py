# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *
from ..Imagen import Imagen


class Comida:
    def __init__(self, mapa):
        self.mapa = mapa
        self.nombre = mapa.nco
        self.punto = mapa.punto

    def dibujar(self, pantalla):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 2:
                    ##Imagen.__init__(self, nombre, x, y, w, h, desx, desy)
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(self.nombre, rect, self.punto, True)
                    imagen.dibujo(pantalla, False)
                lcolum += 1
            lfila += 1