# -*- coding: utf-8 -*-
import pygame
from ..Imagen import Imagen


class Casa:
    def __init__(self, mapa, ):
        self.mapa = mapa
        self.nombre = mapa.nca
        self.punto = mapa.punto

    def dibujar(self, pantalla):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 3:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(self.nombre, rect, self.punto, True)
                    imagen.dibujo(pantalla, False)
                lcolum += 1
            lfila += 1