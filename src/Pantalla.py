#!/usr/bin/python
# -*- coding: utf-8 -*-


import pygame
from pygame.locals import *
from Personaje import *
from Punto import *
from Mapa import *
from Enemigo import *
from DetectorTeclado import *
from Mundo import *
from PanelJuego import PanelJuego
from PanelInicio import PanelInicio
from PanelMapa import PanelMapa
#from Archivo import LeerArchivo
from Archivo import EscribirArchivo
import numpy as np


class Pantalla:
    def __init__(self):
        #Prueba de lectura de mapa
        #lectura = LeerArchivo('mundo.txt')
        #print (lectura.getMensaje())
        #print (lectura.lecturaMat())

        #condicional para cambio de panel
        condicional = 1
        #sección de inicialización
        self.ANCHO = 500
        self.ALTO = 500
        self.escala = 50
        self.salir = False
        self.reloj = pygame.time.Clock()
        pygame.init()
        self.dimension = [self.ANCHO, self.ALTO]
        self.pantalla = pygame.display.set_mode(self.dimension)
        pygame.display.set_caption("Juego nro1 Pygame")
        #matriz del mundo
        #genera comportamiento extraño al poner el jugador en la ultima fila
        self.matriz = [
              [3, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
              [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 3],
              [3, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]]
        #self.guardar = EscribirArchivo('ejemplo.txt', self.matriz)
        #print (self.guardar.getMensaje())
        self.mapa = Mapa(self.matriz, condicional, self.pantalla)
        #self.pj = PanelJuego(self.mapa, self.dimension, self.pantalla)
        self.pj = self.iniciarPanel()

    def iniciar(self):
        while self.salir is not True:
            punto = pygame.mouse.get_pos()
            self.pj.establecerPunto(punto)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salir = True
            if self.salir is not True:
                #detecta posición de la flecha del mouse
                posicion = pygame.mouse.get_pos()
                self.pj.establecerPunto(posicion)
                #panel de juego
                if self.pj.getId() is 2:
                    if self.mapa.getCondicional() is 1:
                        self.reloj.tick(10)
                        self.pj.dibujar()
                    else:
                        self.salir = True
                if self.pj.getId() is 1:
                    self.reloj.tick(10)
                    self.pj.dibujar()
                    #coordenadas donde hizo clic
                    if pygame.mouse.get_pressed()[0] is 1:
                        if self.pj.evento(posicion) is 1:
                            #inicio de juego
                            self.pj = self.iniciarJuego()

                        elif self.pj.evento(posicion) is 2:
                            self.x = x = 7
                            self.y = y = 6
                            #el de la izquierda es y
                            #el de la derecha es x
                            self.matriz = np.zeros([y, x], int)
                            self.matriz[1][1] = 1
                            self.ANCHO = (x + 1) * 50
                            self.ALTO = (y + 1) * 50
                            self.dimension = [self.ANCHO, self.ALTO]
                            self.pantalla = self.display()
                            self.mapa = Mapa(self.matriz, 1, self.pantalla)
                            self.pj = self.iniciarMapa()
                elif self.pj.getId() is 3:
                    self.reloj.tick(10)
                    self.pj.dibujar()

                self.actualizar()
        pygame.quit()

    def actualizar(self):
        pygame.display.update()

    def display(self):
        return pygame.display.set_mode(self.dimension)

    def iniciarJuego(self):
        return PanelJuego(self.mapa, self.dimension, self)

    def iniciarPanel(self):
        return PanelInicio(self.mapa, self.dimension, self.pantalla)

    def iniciarMapa(self):
        return PanelMapa(self.mapa, self.dimension, self)



