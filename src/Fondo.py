# -*- coding: utf-8 -*-
from modelo import *


class Fondo(Imagen):
    def __init__(self, mapa):
        Imagen.__init__(self, mapa.nfondo, mapa.ventana, mapa.punto, False)

    def dibujar(self, pantalla):
        self.dibujo(pantalla, True)