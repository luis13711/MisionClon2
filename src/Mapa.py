# -*- coding: utf-8 -*-
from Punto import *
import pygame
from pygame.locals import *

#// 0 es vacio
#// 1 jugador
#//2 es comida
#//3 es obstaculos
#//4 son enemigos


class Mapa:
    def __init__(self, matriz, condicional, pantalla):
        self.matriz = matriz
        ymatriz = self.filas()
        xmatriz = self.columnas()
        #tamaño bidimensional del mapa
        self.ventana = pygame.Rect(0, 0, xmatriz * 50, ymatriz * 50)
        #punto de desplazamiento de camara
        self.punto = Punto(0, 0)
        self.escala = 50
        self.nfondo = 'img/fondo.png'
        self.nco = 'img/cereza.png'
        self.nca = 'img/edificio.png'
        self.nju = 'img/jugador.png'
        self.nvi = 'img/villano.png'
        self.contador = self.conteoComida()
        self.condicional = condicional
        self.pantalla = pantalla

    def getCondicional(self):
        return self.condicional

    def remover(self, i, j):
        self.matriz[i][j] = 0

    def filas(self):
        return len(self.matriz)

    def columnas(self):
        return len(self.matriz[0])

    def conteoComida(self):
        contador = 0
        lfila = 0
        for fila in self.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 2:
                    contador += 1
                lcolum += 1
            lfila += 1
        return contador

    def getContador(self):
        return self.contador

    def comio(self):
        self.contador -= 1
        if self.contador is 0:
            self.condicional = 0
