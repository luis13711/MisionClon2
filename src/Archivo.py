#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
#from numpy import matrix


class LeerArchivo:
    def __init__(self, nombreArchivo):
        self.nombreArchivo = nombreArchivo
        f = open(nombreArchivo, 'r')
        self.mensaje = f.read()
        filas = 10
        columnas = 10
        #matriz = np.zeros([n,n],int)
        self.matriz = np.zeros((filas, columnas))
        self.matriz = []
        f.close()

    def lecturaMat(self):
        datos = np.loadtxt(self.nombreArchivo, delimiter=' ')
        return datos

    def agregar(self):
        for i in range(10):
            for j in range(10):
                self.matriz[i][j] = 0

    def getMensaje(self):
        return self.mensaje


class EscribirArchivo:
    def __init__(self, nombreArchivo, matriz):
        self.nombreArchivo = nombreArchivo
        self.mensaje = ''
        self.matriz = matriz
        for fila in matriz:
            for columna in fila:
                self.mensaje += str(columna) + ' '
            self.mensaje += '\n'
        f = open(nombreArchivo, 'w')
        f.write(self.mensaje)
        f.close()

    def getMensaje(self):
        return self.mensaje
