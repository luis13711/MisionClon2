# -*- coding: utf-8 -*-
from pygame.locals import *
import pygame

blanco = (255, 255, 255)
rojo = (255, 0, 0)
azul = (0, 0, 255)
negro = (0, 0, 0)


class CuadradoVacio:
    def __init__(self, rect):
        self.rect = rect

    def dibujar(self, pantalla):
        pygame.draw.rect(pantalla, blanco, self.rect, 1)

