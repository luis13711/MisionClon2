# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *
from Personaje import *
from Punto import *
from Mapa import *
from Enemigo import *
from DetectorTeclado import *
from Fondo import *
from Cuadrado import *


class MMundoMapa:
    def __init__(self, mapa, dimension, pantalla):
        #nombres de componentes de juego
        #dimension de camara
        self.dimension = dimension
        #pantalla
        self.pantalla = pantalla
        #mapa con componentes del juego
        self.mapa = mapa
        #personaje del mundo
        self.personaje = PersonajeMovimiento(self.mapa)
        #verificara si se pulso una tecla
        self.detector = DetectorTeclado(self.personaje)

        self.punto = mapa.punto
        #variables de desplazamiento eje x
        self.enteroi = dimension[0] / 2
        ventana = mapa.ventana
        self.enteroe = ventana.width - self.enteroi
        #variables de desplazamiento eje y
        self.enteroiy = dimension[1] / 2
        self.enteroey = ventana.height - self.enteroiy
        #boton que selecciono
        self.boton = None

    def botonSeleccionado(self, numero):
        self.boton = numero

    def actualizar(self):
        if pygame.mouse.get_pressed()[0] is 1:
            punto = pygame.mouse.get_pos()
            rect = pygame.Rect(punto[0], punto[1], 5, 5)
            #print (rect.x, rect.y, rect.width, rect.height)
        #print(len(self.mapa.matriz))
        #print(len(self.mapa.matriz[0]))
        #maty = len(self.mapa.matriz)
        #matx = len(self.mapa.matriz[0])
        #lfila = 1
        #for fila in self.mapa.matriz:
            #lcolum = 1
            #for columna in fila:
                #rect = pygame.Rect(lfila * 50, lcolum * 50, 50, 50)
                #cuadrado = CuadradoVacio(rect)
                #cuadrado.dibujar(self.pantalla)
                #lcolum += 1
            #lfila += 1
        # si no se agrega el más 1 queda un grupo de cuadrados sin pintarse
        lfila = 1
        for fila in range(len(self.mapa.matriz) + 1):
            lcolum = 1
            for columna in range(len(self.mapa.matriz[0]) + 1):
                rect = pygame.Rect(lfila * 50, lcolum * 50, 50, 50)
                cuadrado = CuadradoVacio(rect)
                cuadrado.dibujar(self.pantalla)
                lcolum += 1
            lfila += 1
        #calculo de actualización para eje x
        var1 = self.personaje.rect.x > self.enteroi
        if  var1 and self.personaje.rect.x < self.enteroe:
            self.punto.x = self.personaje.rect.x - self.enteroi

        if self.personaje.rect.x < self.enteroi:
            self.punto.x = 0

        #calculo de actualización para eje y
        var2 = self.personaje.rect.y > self.enteroiy
        if  var2 and self.personaje.rect.y < self.enteroey:
            self.punto.y = self.personaje.rect.y - self.enteroiy

        if self.personaje.rect.y < self.enteroiy:
            self.punto.y = 0


class VMundoMapa:
    def __init__(self, mmundo):
        self.mmundo = mmundo


class CMundoMapa:
    def __init__(self, mmundo, vmundo):
        self.mmundo = mmundo
        self.vmundo = vmundo

    def actualiza(self, pantalla):
        self.mmundo.detector.mover(pygame.key.get_pressed())
        #self.cmundo.actualiza()
        self.mmundo.actualizar()
        #self.vmundo.dibujar(pantalla)


class MundoMapa:
    def __init__(self, mapa, dimension, pantalla):
        mmundo = MMundoMapa(mapa, dimension, pantalla)
        vmundo = VMundoMapa(mmundo)
        self.cmundo = CMundoMapa(mmundo, vmundo)

    def actualiza(self, pantalla):
        self.cmundo.actualiza(pantalla)
