# -*- coding: utf-8 -*-
from Mapa import Mapa
import pygame
from pygame.locals import *
import numpy as np
from MundoMapa import MundoMapa


class Guardador:
    def __init__(self, Pantalla, panel):
        self.Pantalla = Pantalla
        self.panel = panel

    def modificar(self):
        #el de la izquierda es y
        #el de la derecha es x
        x = self.Pantalla.x
        y = self.Pantalla.y
        self.Pantalla.mapa.matriz = np.zeros([y, x], int)
        matriz = self.Pantalla.mapa.matriz
        #self.matriz[1][1] = 1
        self.ANCHO = (x + 1) * 50
        self.ALTO = (y + 1) * 50
        self.dimension = [self.ANCHO, self.ALTO]
        self.Pantalla.pantalla = self.display()
        self.Pantalla.mapa = Mapa(matriz, 1, self.Pantalla.pantalla)
        mapa = self.Pantalla.mapa
        pantalla = self.Pantalla.pantalla
        self.panel.mundo = MundoMapa(mapa, self.dimension, pantalla)

    def aumentarX(self):
        self.Pantalla.x += 1
        self.modificar()

    def aumentarY(self):
        self.Pantalla.y += 1
        self.modificar()

    def decrementarX(self):
        #self.x = x = 7
        if not self.Pantalla.x is 7:
            self.Pantalla.x -= 1
            self.modificar()

    def decrementarY(self):
        #self.y = y = 6
        if not self.Pantalla.y is 6:
            self.Pantalla.y -= 1
            self.modificar()

    def display(self):
        return pygame.display.set_mode(self.dimension)