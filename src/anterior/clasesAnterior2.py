#!/usr/bin/python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-
#

import pygame
import sys
from pygame.locals import *
from Tkconstants import FALSE


class Desplazamiento:
    def __init__(self, entero):
        self.entero = entero


class Punto:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Imagen:
    def __init__(self, nombre, rect, desx, desy, transparente):
        #coordenadas de imagen
        self.rect = rect
        #imagen
        self.imagen = self.imagen(nombre, transparente)
        #desplazamiento eje x
        self.desplazamientox = desx
        #desplazamiento eje y
        self.desplazamientoy = desy

    def imagen(self, filename, transparent=False):
        try:
            image = pygame.image.load(filename)
            punto = (self.rect.width, self.rect.height)
            image = pygame.transform.scale(image, punto)
        except pygame.error as message:
                    raise SystemExit(message)
        image = image.convert()
        if transparent:
            color = image.get_at((0, 0))
            image.set_colorkey(color, RLEACCEL)
        return image

    def dibujo(self, pantalla, recortar):
        if recortar is True:
            #imagen recortada
            x = self.rect.x + self.desplazamientox.entero
            y = self.rect.y + self.desplazamientoy.entero
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, (0, 0), temp)
        else:
            #desplazamiento eje x
            x = self.rect.x - self.desplazamientox.entero
            #desplazamiento eje y
            y = self.rect.y - self.desplazamientoy.entero
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, temp)


class Fondo(Imagen):
    def __init__(self, x, y, w, h, nombre, desx, desy):
        rect = pygame.Rect(x, y, w, h)
        Imagen.__init__(self, nombre, rect, desx, desy, False)

    def dibujar(self, pantalla):
        self.dibujo(pantalla, True)


class Personaje(Imagen):
    def __init__(self, matriz, nombre, x1, y1, w1, h1, desx, desy, tran):
        self.matriz = matriz
        #dimensiones del mundo
        self.x = x1
        self.y = y1
        self.w = w1
        self.h = h1
        #ubicación del jugador
        rect = self.inicializar()
        #(x, y, w, h) = (rect.x, rect.y, rect.width, rect.height)
        Imagen.__init__(self, nombre, rect, desx, desy, tran)

    def inicializar(self):
        lfila = 0
        rect = None
        for fila in self.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 1:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                lcolum += 1
            lfila += 1
        return rect

    def move(self, entero):
        #derecha
        if entero == 1:
            self.rect.x += 10
            tempx = self.rect.x + self.rect.width
            if tempx > self.w:
                self.rect.x = self.w - self.rect.width

        #izquierda
        if entero == 2:
            self.rect.x -= 10
            if self.rect.x < self.x:
                self.rect.x = self.x
        #arriba
        if entero == 3:
            self.rect.y -= 10
            if self.rect.y < self.y:
                self.rect.y = self.y
        #abajo
        if entero == 4:
            self.rect.y += 10
            tempy = self.rect.y + self.rect.width
            if tempy > self.h:
                self.rect.y = self.h - self.rect.height

    def dibujar(self, pantalla):
        self.dibujo(pantalla, False)


class Jugador(Personaje):
    def __init__(self, matriz, x1, y1, w1, h1, desx, desy):
        nju = 'img/jugador.png'
        Personaje.__init__(self, matriz, nju, x1, y1, w1, h1, desx, desy, True)

    def mover(self, teclado):
        if teclado[pygame.K_RIGHT]:
            self.move(1)
        if teclado[pygame.K_LEFT]:
            self.move(2)
        if teclado[pygame.K_UP]:
            self.move(3)
        if teclado[pygame.K_DOWN]:
            self.move(4)


class Muro:
    def __init__(self, matriz, nombre, w, h, desx, desy):
        self.matriz = matriz
        self.lista = []
        self.w = w
        self.h = h
        lfila = 0
        for fila in self.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 3:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(nombre, rect, desx, desy, True)
                    self.lista.append(imagen)
                lcolum += 1
            lfila += 1
        self.desplazamientox = desx
        self.desplazamientoy = desy

    def dibujar(self, pantalla):
        for imagen in self.lista:
            imagen.dibujo(pantalla, False)


class Comida:
    def __init__(self, matriz, nombre, w, h, desx, desy):
        self.matriz = matriz
        self.lista = []
        self.w = w
        self.h = h
        lfila = 0
        for fila in self.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 2:
                    ##Imagen.__init__(self, nombre, x, y, w, h, desx, desy)
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(nombre, rect, desx, desy, True)
                    self.lista.append(imagen)
                lcolum += 1
            lfila += 1

        self.desplazamientox = desx
        self.desplazamientoy = desy

    def dibujar(self, pantalla):
        for imagen in self.lista:
            imagen.dibujo(pantalla, False)


class CMundo:
    def __init__(self, desx, desy, dimension, taman, jugador):
        self.desplazamientox = desx
        self.desplazamientoy = desy

        #variables de desplazamiento eje x
        self.enteroi = dimension[0] / 2
        self.enteroe = taman.width - self.enteroi
        #variables de desplazamiento eje y
        self.enteroiy = dimension[1] / 2
        self.enteroey = taman.height - self.enteroiy
        #jugador
        self.jugador = jugador

    def actualiza(self):
        #calculo de actualización para eje x
        var1 = self.jugador.rect.x > self.enteroi
        if  var1 and self.jugador.rect.x < self.enteroe:
            self.desplazamientox.entero = self.jugador.rect.x - self.enteroi
        if self.jugador.rect.x < self.enteroi:
            self.desplazamientox.entero = 0
        #calculo de actualización para eje y
        var2 = self.jugador.rect.y > self.enteroiy
        if  var2 and self.jugador.rect.y < self.enteroey:
            self.desplazamientoy.entero = self.jugador.rect.y - self.enteroiy
        if self.jugador.rect.y < self.enteroiy:
            self.desplazamientoy.entero = 0


class Colisiones:
    def __init__(self, jugador, listacomida):
        self.jugador = jugador
        self.listacomida = listacomida

    def initAudio(self):
        self.sonidoRecoleccion = pygame.mixer.Sound('recoleccion.wav')

    def actualizar(self):
        temp = None
        colision = False
        for comida in self.listacomida.lista:
            if self.jugador.rect.colliderect(comida):
                temp = comida
                colision = True
                self.sonidoRecoleccion.play()
        if colision:
            self.listacomida.lista.remove(temp)


class Mundo:
    def __init__(self, matriz, dimension):
        #nombres de componentes de juego
        nfondo = 'img/fondo_bob_esponja.gif'
        nco = 'img/cereza.png'
        #nju = 'img/jugador.png'
        #matriz con componentes del juego
        self.matriz = matriz
        ymatriz = len(self.matriz)
        xmatriz = len(self.matriz[0])
        #tamaniom = pygame.Rect(0, 0, xmatriz * 50, 500)
        taman = pygame.Rect(0, 0, xmatriz * 50, ymatriz * 50)
        #desplazamiento en el eje x
        self.desx = Desplazamiento(0)
        #desplazamiento en el eje y
        self.desy = Desplazamiento(0)
        self.tamaniom = taman
        x = taman.x
        y = taman.y
        w = taman.width
        h = taman.height
        #x,y,wfondo,hfondo
        #wv, hv
        wv = dimension[0]
        hv = dimension[1]
        self.fondo = Fondo(x, y, w, h, nfondo, self.desx, self.desy)
        self.listacomida = Comida(self.matriz, nco, w, h, self.desx, self.desy)
        #self.juga = Jugador(self.matriz, nju, x, y, w, h, self.desx, self.desy)
        self.juga = Jugador(self.matriz, x, y, w, h, self.desx, self.desy)
        #self.cmundo = CMundo(self.desx, self.desy, dimension, w, self.jugador)
        self.cmundo = CMundo(self.desx, self.desy, dimension, taman, self.juga)
        self.colision = Colisiones(self.juga, self.listacomida)
        self.colision.initAudio()


class PanelJuego:
    def __init__(self, matriz, dimension, pantalla):
        #sección de componentes del juego
        self.matriz = matriz
        self.dimension = dimension
        #escala de juego 50
        #tamaniom = pygame.Rect(0, 0, 2000, 500)
        self.mundo = Mundo(self.matriz, self.dimension)
        self.listacomida = self.mundo.listacomida
        self.jugador = self.mundo.juga
        self.fondo = self.mundo.fondo
        self.cmundo = self.mundo.cmundo
        self.colision = self.mundo.colision
        self.pantalla = pantalla
        self.cmundo.actualiza()

    def dibujar(self):
        self.jugador.mover(pygame.key.get_pressed())
        self.cmundo.actualiza()
        self.colision.actualizar()
        self.fondo.dibujar(self.pantalla)
        self.listacomida.dibujar(self.pantalla)
        self.jugador.dibujar(self.pantalla)


class Pantalla:
    def __init__(self):
        #sección de inicialización
        self.ANCHO = 500
        self.ALTO = 500
        self.escala = 50
        self.salir = False
        self.reloj = pygame.time.Clock()
        pygame.init()
        self.dimension = [self.ANCHO, self.ALTO]
        self.pantalla = pygame.display.set_mode(self.dimension)
        pygame.display.set_caption("Juego nro1 Pygame")
        #matriz del mundo
        #genera comportamiento extraño al poner el jugador en la ultima fila
        self.matriz = [
              [0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2],
              [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

        self.pj = PanelJuego(self.matriz, self.dimension, self.pantalla)
        while self.salir is not True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salir = True
            if self.salir is not True:
                self.reloj.tick(20)
                self.pj.dibujar()
                pygame.display.update()
        pygame.quit()
