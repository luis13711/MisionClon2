#!/usr/bin/python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-
#

import pygame
import sys
from pygame.locals import *
from Tkconstants import FALSE


class Punto:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Imagen:
    def __init__(self, nombre, rect, punto, transparente):
        #coordenadas de imagen
        self.rect = rect
        #imagen
        self.imagen = self.imagen(nombre, transparente)
        self.punto = punto

    def imagen(self, filename, transparent=False):
        try:
            image = pygame.image.load(filename)
            punto = (self.rect.width, self.rect.height)
            image = pygame.transform.scale(image, punto)
        except pygame.error as message:
                    raise SystemExit(message)
        image = image.convert()
        if transparent:
            color = image.get_at((0, 0))
            image.set_colorkey(color, RLEACCEL)
        return image

    def dibujo(self, pantalla, recortar):
        if recortar is True:
            #imagen recortada
            x = self.rect.x + self.punto.x
            y = self.rect.y + self.punto.y
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, (0, 0), temp)
        else:
            #desplazamiento eje x
            x = self.rect.x - self.punto.x
            #desplazamiento eje y
            y = self.rect.y - self.punto.y
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, temp)


class Fondo(Imagen):
    def __init__(self, ventana, nombre, punto):
        Imagen.__init__(self, nombre, ventana, punto, False)

    def dibujar(self, pantalla):
        self.dibujo(pantalla, True)


class Personaje(Imagen):
    def __init__(self, matriz, nombre, ventana, punto, tran):
        self.matriz = matriz
        #dimensiones del mundo
        self.ventana = ventana
        #ubicación del personaje
        lfila = 0
        for fila in self.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 1:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    Imagen.__init__(self, nombre, rect, punto, tran)
                lcolum += 1
            lfila += 1

    def move(self, entero):
        #derecha
        if entero == 1:
            self.rect.x += 10
            tempx = self.rect.x + self.rect.width
            if tempx > self.ventana.width:
                self.rect.x = self.ventana.width - self.rect.width

        #izquierda
        if entero == 2:
            self.rect.x -= 10
            if self.rect.x < self.ventana.x:
                self.rect.x = self.ventana.x
        #arriba
        if entero == 3:
            self.rect.y -= 10
            if self.rect.y < self.ventana.y:
                self.rect.y = self.ventana.y
        #abajo
        if entero == 4:
            self.rect.y += 10
            tempy = self.rect.y + self.rect.width
            if tempy > self.ventana.height:
                self.rect.y = self.ventana.height - self.rect.height

    def dibujar(self, pantalla):
        self.dibujo(pantalla, False)


class Jugador(Personaje):
    def __init__(self, matriz, ventana, punto):
        nju = 'img/jugador.png'
        Personaje.__init__(self, matriz, nju, ventana, punto, True)

    def mover(self, teclado):
        if teclado[pygame.K_RIGHT]:
            self.move(1)
        if teclado[pygame.K_LEFT]:
            self.move(2)
        if teclado[pygame.K_UP]:
            self.move(3)
        if teclado[pygame.K_DOWN]:
            self.move(4)


class DetectorTeclado:
    def __init__(self, jugador, colisionescasa):
        self.jugador = jugador
        self.colisionescasa = colisionescasa

    def mover(self, teclado):
        if teclado[pygame.K_RIGHT]:
            self.jugador.move(1)
        if teclado[pygame.K_LEFT]:
            self.jugador.move(2)
        if teclado[pygame.K_UP]:
            self.jugador.move(3)
        if teclado[pygame.K_DOWN]:
            self.jugador.move(4)


class Casa:
    def __init__(self, matriz, nombre, ventana, punto):
        self.matriz = matriz
        self.lista = []
        #self.w = w
        #self.h = h
        lfila = 0
        for fila in self.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 3:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(nombre, rect, punto, True)
                    self.lista.append(imagen)
                lcolum += 1
            lfila += 1

    def dibujar(self, pantalla):
        for imagen in self.lista:
            imagen.dibujo(pantalla, False)


class Comida:
    def __init__(self, matriz, nombre, ventana, punto):
        self.matriz = matriz
        self.lista = []
        #self.w = w
        #self.h = h
        lfila = 0
        for fila in self.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 2:
                    ##Imagen.__init__(self, nombre, x, y, w, h, desx, desy)
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(nombre, rect, punto, True)
                    self.lista.append(imagen)
                lcolum += 1
            lfila += 1

    def dibujar(self, pantalla):
        for imagen in self.lista:
            imagen.dibujo(pantalla, False)


class CMundo:
    def __init__(self, punto, dimension, ventana, jugador):
        #self.desplazamientox = desx
        #self.desplazamientoy = desy
        self.punto = punto
        #variables de desplazamiento eje x
        self.enteroi = dimension[0] / 2
        self.enteroe = ventana.width - self.enteroi
        #variables de desplazamiento eje y
        self.enteroiy = dimension[1] / 2
        self.enteroey = ventana.height - self.enteroiy
        #jugador
        self.jugador = jugador

    def actualiza(self):
        #calculo de actualización para eje x
        var1 = self.jugador.rect.x > self.enteroi
        if  var1 and self.jugador.rect.x < self.enteroe:
            self.punto.x = self.jugador.rect.x - self.enteroi

        if self.jugador.rect.x < self.enteroi:
            self.punto.x = 0

        #calculo de actualización para eje y
        var2 = self.jugador.rect.y > self.enteroiy
        if  var2 and self.jugador.rect.y < self.enteroey:
            self.punto.y = self.jugador.rect.y - self.enteroiy

        if self.jugador.rect.y < self.enteroiy:
            self.punto.y = 0


class Colisiones:
    def __init__(self, jugador, listacomida):
        self.jugador = jugador
        self.listacomida = listacomida

    def initAudio(self):
        self.sonidoRecoleccion = pygame.mixer.Sound('recoleccion.wav')

    def actualizar(self):
        temp = None
        colision = False
        for comida in self.listacomida.lista:
            if self.jugador.rect.colliderect(comida):
                temp = comida
                colision = True
                self.sonidoRecoleccion.play()
        if colision:
            self.listacomida.lista.remove(temp)


class ColisionCasa:
    def __init__(self, jugador, listacasa):
        self.jugador = jugador
        self.lista = listacasa.lista

    def initAudio(self):
        self.sonidoRecoleccion = pygame.mixer.Sound('recoleccion.wav')

    #verifica si colisiona a la izquierda
    def izquierda(self):
        temp = None
        colision = False
        for casa in self.lista:
            if self.jugador.rect.colliderect(comida):
                temp = comida
                colision = True
        if colision:
            #self.listacomida.lista.remove(temp)
            while colision:
                self.jugadador.rect.x += 1
                if self.jugador.rect.colliderect(temp):
                    colision = True
                else:
                    colision = False

    def derecha(self):
        temp = None
        colision = False
        for casa in self.lista:
            if self.jugador.rect.colliderect(comida):
                temp = comida
                colision = True
        if colision:
            #self.listacomida.lista.remove(temp)
            while colision:
                self.jugadador.rect.x -= 1
                if self.jugador.rect.colliderect(temp):
                    colision = True
                else:
                    colision = False

    def arriba(self):
        temp = None
        colision = False
        for casa in self.lista:
            if self.jugador.rect.colliderect(comida):
                temp = comida
                colision = True
        if colision:
            #self.listacomida.lista.remove(temp)
            while colision:
                self.jugadador.rect.y += 1
                if self.jugador.rect.colliderect(temp):
                    colision = True
                else:
                    colision = False

    def abajo(self):
        temp = None
        colision = False
        for casa in self.lista:
            if self.jugador.rect.colliderect(comida):
                temp = comida
                colision = True
        if colision:
            #self.listacomida.lista.remove(temp)
            while colision:
                self.jugadador.rect.y -= 1
                if self.jugador.rect.colliderect(temp):
                    colision = True
                else:
                    colision = False


class Mundo:
    def __init__(self, matriz, dimension, pantalla):
        #nombres de componentes de juego
        nfondo = 'img/fondo_bob_esponja.gif'
        nco = 'img/cereza.png'
        nca = 'img/edificio.png'
        #nju = 'img/jugador.png'
        #pantalla
        self.pantalla = pantalla
        #matriz con componentes del juego
        self.matriz = matriz
        ymatriz = len(self.matriz)
        xmatriz = len(self.matriz[0])
        #tamanio de ventana = pygame.Rect(0, 0, xmatriz * 50, 500)
        ventana = pygame.Rect(0, 0, xmatriz * 50, ymatriz * 50)

        #punto para desplazamiento
        punto = Punto(0, 0)
        #fondo del mundo
        self.fondo = Fondo(ventana, nfondo, punto)
        #comida del mundo
        self.listacomida = Comida(self.matriz, nco, ventana, punto)
        #casas del mundo
        self.casas = Casa(self.matriz, nca, ventana, punto)
        #jugador del mundo
        self.jugador = Jugador(self.matriz, ventana, punto)
        #controlador del mundo
        self.cmundo = CMundo(punto, dimension, ventana, self.jugador)
        #colisiones con comida
        self.colision = Colisiones(self.jugador, self.listacomida)
        #colisiones con objetos del mundo
        colisioncasa = ColisionCasa(self.jugador, self.casas)
        #verificara si colisiona

        self.colision.initAudio()

    def dibujar(self):
        self.jugador.mover(pygame.key.get_pressed())
        self.cmundo.actualiza()
        self.colision.actualizar()
        self.fondo.dibujar(self.pantalla)
        self.listacomida.dibujar(self.pantalla)
        self.jugador.dibujar(self.pantalla)
        self.casas.dibujar(self.pantalla)


class PanelJuego:
    def __init__(self, matriz, dimension, pantalla):
        #sección de componentes del juego
        self.matriz = matriz
        self.dimension = dimension
        #escala de juego 50
        #tamaniom = pygame.Rect(0, 0, 2000, 500)
        self.pantalla = pantalla
        self.mundo = Mundo(self.matriz, self.dimension, pantalla)

    def dibujar(self):
        self.mundo.dibujar()


class Pantalla:
    def __init__(self):
        #sección de inicialización
        self.ANCHO = 500
        self.ALTO = 500
        self.escala = 50
        self.salir = False
        self.reloj = pygame.time.Clock()
        pygame.init()
        self.dimension = [self.ANCHO, self.ALTO]
        self.pantalla = pygame.display.set_mode(self.dimension)
        pygame.display.set_caption("Juego nro1 Pygame")
        #matriz del mundo
        #genera comportamiento extraño al poner el jugador en la ultima fila
        self.matriz = [
              [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
              [3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]]

        self.pj = PanelJuego(self.matriz, self.dimension, self.pantalla)
        while self.salir is not True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salir = True
            if self.salir is not True:
                self.reloj.tick(20)
                self.pj.dibujar()
                pygame.display.update()
        pygame.quit()

