#!/usr/bin/python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-
#

import pygame
import sys
from pygame.locals import *
from Tkconstants import FALSE


class Punto:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Mapa:
    def __init__(self, matriz):
        self.matriz = matriz
        ymatriz = self.filas()
        xmatriz = self.columnas()
        #tamaño bidimensional del mapa
        self.ventana = pygame.Rect(0, 0, xmatriz * 50, ymatriz * 50)
        #punto de desplazamiento de camara
        self.punto = Punto(0, 0)

    def remover(self, i, j):
        self.matriz[i][j] = 0

    def filas(self):
        return len(self.matriz)

    def columnas(self):
        return len(self.matriz[0])


class Imagen:
    def __init__(self, nombre, rect, punto, transparente):
        #coordenadas de imagen
        self.rect = rect
        #imagen
        self.imagen = self.imagen(nombre, transparente)
        self.punto = punto

    def imagen(self, filename, transparent=False):
        try:
            image = pygame.image.load(filename)
            punto = (self.rect.width, self.rect.height)
            image = pygame.transform.scale(image, punto)
        except pygame.error as message:
                    raise SystemExit(message)
        image = image.convert()
        if transparent:
            color = image.get_at((0, 0))
            image.set_colorkey(color, RLEACCEL)
        return image

    def dibujo(self, pantalla, recortar):
        if recortar is True:
            #imagen recortada
            x = self.rect.x + self.punto.x
            y = self.rect.y + self.punto.y
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, (0, 0), temp)
        else:
            #desplazamiento eje x
            x = self.rect.x - self.punto.x
            #desplazamiento eje y
            y = self.rect.y - self.punto.y
            temp = pygame.Rect(x, y, self.rect.width, self.rect.height)
            pantalla.blit(self.imagen, temp)


class Fondo(Imagen):
    def __init__(self, mapa, nombre):
        Imagen.__init__(self, nombre, mapa.ventana, mapa.punto, False)

    def dibujar(self, pantalla):
        self.dibujo(pantalla, True)


class Personaje(Imagen):
    def __init__(self, mapa, nombre, tran):
        self.mapa = mapa
        #dimensiones del mundo
        self.ventana = mapa.ventana
        #ubicación del personaje
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 1:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    Imagen.__init__(self, nombre, rect, mapa.punto, tran)
                lcolum += 1
            lfila += 1
            #vida del personaje
            self.vida = 100
            #puntuación
            self.puntuacion = 0

    def move(self, entero):
        #derecha
        if entero == 1:
            self.rect.x += 10
            tempx = self.rect.x + self.rect.width
            if tempx > self.ventana.width:
                self.rect.x = self.ventana.width - self.rect.width

        #izquierda
        if entero == 2:
            self.rect.x -= 10
            if self.rect.x < self.ventana.x:
                self.rect.x = self.ventana.x
        #arriba
        if entero == 3:
            self.rect.y -= 10
            if self.rect.y < self.ventana.y:
                self.rect.y = self.ventana.y
        #abajo
        if entero == 4:
            self.rect.y += 10
            tempy = self.rect.y + self.rect.width
            if tempy > self.ventana.height:
                self.rect.y = self.ventana.height - self.rect.height

    def derecha(self):
        #derecha
        self.rect.x += 10
        tempx = self.rect.x + self.rect.width
        if tempx > self.ventana.width:
            self.rect.x = self.ventana.width - self.rect.width

    def izquierda(self):
        #izquierda
        self.rect.x -= 10
        if self.rect.x < self.ventana.x:
            self.rect.x = self.ventana.x

    def arriba(self):
        #arriba
        self.rect.y -= 10
        if self.rect.y < self.ventana.y:
            self.rect.y = self.ventana.y

    def abajo(self):
        #abajo
        self.rect.y += 10
        tempy = self.rect.y + self.rect.width
        if tempy > self.ventana.height:
            self.rect.y = self.ventana.height - self.rect.height

    def dibujar(self, pantalla):
        self.dibujo(pantalla, False)


class Jugador:
    def __init__(self, personaje, colisionescasa, colisioncomida):
        self.personaje = personaje
        self.colisionescasa = colisionescasa
        self.colisioncomida = colisioncomida

    def derecha(self):
        self.personaje.derecha()
        self.colisionescasa.derecha(self.personaje)
        self.colisioncomida.actualizar(self.personaje)

    def izquierda(self):
        self.personaje.izquierda()
        self.colisionescasa.izquierda(self.personaje)
        self.colisioncomida.actualizar(self.personaje)

    def arriba(self):
        self.personaje.arriba()
        self.colisionescasa.arriba(self.personaje)
        self.colisioncomida.actualizar(self.personaje)

    def abajo(self):
        self.personaje.abajo()
        self.colisionescasa.abajo(self.personaje)
        self.colisioncomida.actualizar(self.personaje)

    def dibujar(self, pantalla):
        self.personaje.dibujar(pantalla)


class DetectorTeclado:
    def __init__(self, jugador):
        self.jugador = jugador

    def mover(self, teclado):
        if teclado[pygame.K_RIGHT]:
            self.jugador.derecha()
        if teclado[pygame.K_LEFT]:
            self.jugador.izquierda()
        if teclado[pygame.K_UP]:
            self.jugador.arriba()
        if teclado[pygame.K_DOWN]:
            self.jugador.abajo()

    def dibujar(self, pantalla):
        self.jugador.dibujar(pantalla)


class Casa:
    def __init__(self, mapa, nombre):
        self.mapa = mapa
        self.nombre = nombre
        self.punto = mapa.punto

    def dibujar(self, pantalla):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 3:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(self.nombre, rect, self.punto, True)
                    imagen.dibujo(pantalla, False)
                lcolum += 1
            lfila += 1
        #for imagen in self.lista:
            #imagen.dibujo(pantalla, False)


class Comida:
    def __init__(self, mapa, nombre):
        self.mapa = mapa
        self.nombre = nombre
        self.punto = mapa.punto

    def dibujar(self, pantalla):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 2:
                    ##Imagen.__init__(self, nombre, x, y, w, h, desx, desy)
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    imagen = Imagen(self.nombre, rect, self.punto, True)
                    imagen.dibujo(pantalla, False)
                lcolum += 1
            lfila += 1
        #for imagen in self.lista:
            #imagen.dibujo(pantalla, False)


class CMundo:
    def __init__(self, mapa, dimension, jugador):
        #self.desplazamientox = desx
        #self.desplazamientoy = desy
        self.punto = mapa.punto
        #variables de desplazamiento eje x
        self.enteroi = dimension[0] / 2
        ventana = mapa.ventana
        self.enteroe = ventana.width - self.enteroi
        #variables de desplazamiento eje y
        self.enteroiy = dimension[1] / 2
        self.enteroey = ventana.height - self.enteroiy
        #jugador
        self.jugador = jugador.personaje

    def actualiza(self):
        #calculo de actualización para eje x
        var1 = self.jugador.rect.x > self.enteroi
        if  var1 and self.jugador.rect.x < self.enteroe:
            self.punto.x = self.jugador.rect.x - self.enteroi

        if self.jugador.rect.x < self.enteroi:
            self.punto.x = 0

        #calculo de actualización para eje y
        var2 = self.jugador.rect.y > self.enteroiy
        if  var2 and self.jugador.rect.y < self.enteroey:
            self.punto.y = self.jugador.rect.y - self.enteroiy

        if self.jugador.rect.y < self.enteroiy:
            self.punto.y = 0


class Colisiones:
    def __init__(self, listacomida):
        self.mapa = listacomida.mapa
        self.sonidoRecoleccion = pygame.mixer.Sound('recoleccion.wav')

    def verificar(self, personaje):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 2:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    if personaje.rect.colliderect(rect):
                        #eliminamos la comida
                        self.mapa.remover(lfila, lcolum)
                        #damos puntos al personaje
                        personaje.puntuacion += 1
                        return True
                lcolum += 1
            lfila += 1
        return False

    def actualizar(self, personaje):
        if self.verificar(personaje):
            self.sonidoRecoleccion.play()


class ColisionCasa:
    def __init__(self, listacasa):
        self.mapa = listacasa.mapa

    def initAudio(self):
        self.sonidoRecoleccion = pygame.mixer.Sound('recoleccion.wav')

    def verificar(self, jugador):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 3:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    if jugador.rect.colliderect(rect):
                        return True
                lcolum += 1
            lfila += 1
        return False

    def izquierda(self, jugador):
        while self.verificar(jugador):
            jugador.rect.x += 1

    def derecha(self, jugador):
        while self.verificar(jugador):
            jugador.rect.x -= 1

    def arriba(self, jugador):
        while self.verificar(jugador):
            jugador.rect.y += 1

    def abajo(self, jugador):
        while self.verificar(jugador):
            jugador.rect.y -= 1


class Mundo:
    def __init__(self, mapa, dimension, pantalla):
        #nombres de componentes de juego
        nfondo = 'img/fondo_bob_esponja.gif'
        nco = 'img/cereza.png'
        nca = 'img/edificio.png'
        #nju = 'img/jugador.png'
        #pantalla
        self.pantalla = pantalla
        #mapa con componentes del juego
        self.mapa = mapa
        #fondo del mundo
        self.fondo = Fondo(mapa, nfondo)
        #comida del mundo
        self.comidas = Comida(self.mapa, nco)
        #colisiones con comida
        colision = Colisiones(self.comidas)
        #casas del mundo
        self.casas = Casa(self.mapa, nca)
        #colisiones con objetos del mundo
        colisioncasa = ColisionCasa(self.casas)
        #personaje del mundo
        name = 'img/jugador.png'
        self.personaje = Personaje(self.mapa, name, True)
        #jugador del mundo
        self.jugador = Jugador(self.personaje, colisioncasa, colision)
        #verificara si se pulso una tecla
        self.detector = DetectorTeclado(self.jugador)
        #controlador del mundo
        #self.cmundo = CMundo(self.mapa, dimension, self.jugador)

        #self.desplazamientox = desx
        #self.desplazamientoy = desy
        self.punto = mapa.punto
        #variables de desplazamiento eje x
        self.enteroi = dimension[0] / 2
        ventana = mapa.ventana
        self.enteroe = ventana.width - self.enteroi
        #variables de desplazamiento eje y
        self.enteroiy = dimension[1] / 2
        self.enteroey = ventana.height - self.enteroiy

    def dibujar(self):
        self.detector.mover(pygame.key.get_pressed())
        #self.cmundo.actualiza()
        self.actualiza()
        #self.colision.actualizar()
        self.fondo.dibujar(self.pantalla)
        self.comidas.dibujar(self.pantalla)
        self.jugador.dibujar(self.pantalla)
        self.casas.dibujar(self.pantalla)

    def actualiza(self):
        #calculo de actualización para eje x
        var1 = self.personaje.rect.x > self.enteroi
        if  var1 and self.personaje.rect.x < self.enteroe:
            self.punto.x = self.personaje.rect.x - self.enteroi

        if self.personaje.rect.x < self.enteroi:
            self.punto.x = 0

        #calculo de actualización para eje y
        var2 = self.personaje.rect.y > self.enteroiy
        if  var2 and self.personaje.rect.y < self.enteroey:
            self.punto.y = self.personaje.rect.y - self.enteroiy

        if self.personaje.rect.y < self.enteroiy:
            self.punto.y = 0


class PanelJuego:
    def __init__(self, mapa, dimension, pantalla):
        #sección de componentes del juego
        self.mapa = mapa
        self.dimension = dimension
        #escala de juego 50
        #tamaniom = pygame.Rect(0, 0, 2000, 500)
        self.pantalla = pantalla
        self.mundo = Mundo(self.mapa, self.dimension, pantalla)

    def dibujar(self):
        self.mundo.dibujar()


class Pantalla:
    def __init__(self):
        #sección de inicialización
        self.ANCHO = 500
        self.ALTO = 500
        self.escala = 50
        self.salir = False
        self.reloj = pygame.time.Clock()
        pygame.init()
        self.dimension = [self.ANCHO, self.ALTO]
        self.pantalla = pygame.display.set_mode(self.dimension)
        pygame.display.set_caption("Juego nro1 Pygame")
        #matriz del mundo
        #genera comportamiento extraño al poner el jugador en la ultima fila
        self.matriz = [
              [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
              [3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
              [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]]
        self.mapa = Mapa(self.matriz)

        self.pj = PanelJuego(self.mapa, self.dimension, self.pantalla)
        while self.salir is not True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salir = True
            if self.salir is not True:
                self.reloj.tick(10)
                self.pj.dibujar()
                pygame.display.update()
        pygame.quit()


