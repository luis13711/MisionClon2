# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *


class ColisionCasa:
    def __init__(self, listacasa):
        self.mapa = listacasa.mapa

    def initAudio(self):
        self.sonidoRecoleccion = pygame.mixer.Sound('recoleccion.wav')

    def verificar(self, jugador):
        lfila = 0
        for fila in self.mapa.matriz:
            lcolum = 0
            for columna in fila:
                if columna == 3:
                    rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                    if jugador.rect.colliderect(rect):
                        return True
                lcolum += 1
            lfila += 1
        return False

    def izquierda(self, jugador):
        while self.verificar(jugador):
            jugador.rect.x += 1

    def derecha(self, jugador):
        while self.verificar(jugador):
            jugador.rect.x -= 1

    def arriba(self, jugador):
        while self.verificar(jugador):
            jugador.rect.y += 1

    def abajo(self, jugador):
        while self.verificar(jugador):
            jugador.rect.y -= 1