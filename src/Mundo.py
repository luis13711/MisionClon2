# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *
from Personaje import *
from Punto import *
from Mapa import *
from Enemigo import *
from DetectorTeclado import *
from Fondo import *


class MMundo:
    def __init__(self, mapa, dimension, pantalla):
        #nombres de componentes de juego
        #dimension de camara
        self.dimension = dimension
        #pantalla
        self.pantalla = pantalla
        #mapa con componentes del juego
        self.mapa = mapa
        #fondo del mundo
        self.fondo = Fondo(mapa)
        #comida del mundo
        self.comidas = Comida(self.mapa)
        #casas del mundo
        self.casas = Casa(self.mapa)
        #personaje del mundo
        self.personaje = Personaje(self.mapa)
        #verificara si se pulso una tecla
        self.detector = DetectorTeclado(self.personaje)

        #enemigo del mundo
        self.cenemigos = CEnemigo(self.mapa)

        self.punto = mapa.punto
        #variables de desplazamiento eje x
        self.enteroi = dimension[0] / 2
        ventana = mapa.ventana
        self.enteroe = ventana.width - self.enteroi
        #variables de desplazamiento eje y
        self.enteroiy = dimension[1] / 2
        self.enteroey = ventana.height - self.enteroiy

    def actualizar(self):
        #calculo de actualización para eje x
        var1 = self.personaje.rect.x > self.enteroi
        if  var1 and self.personaje.rect.x < self.enteroe:
            self.punto.x = self.personaje.rect.x - self.enteroi

        if self.personaje.rect.x < self.enteroi:
            self.punto.x = 0

        #calculo de actualización para eje y
        var2 = self.personaje.rect.y > self.enteroiy
        if  var2 and self.personaje.rect.y < self.enteroey:
            self.punto.y = self.personaje.rect.y - self.enteroiy

        if self.personaje.rect.y < self.enteroiy:
            self.punto.y = 0


class VMundo:
    def __init__(self, mmundo):
        self.mmundo = mmundo

    def dibujar(self, pantalla):
        self.mmundo.fondo.dibujar(pantalla)
        self.mmundo.comidas.dibujar(pantalla)
        self.mmundo.personaje.dibujar(pantalla)
        self.mmundo.casas.dibujar(pantalla)
        self.mmundo.cenemigos.dibujar(pantalla)


class CMundo:
    def __init__(self, mmundo, vmundo):
        self.mmundo = mmundo
        self.vmundo = vmundo

    def actualiza(self, pantalla):
        self.mmundo.detector.mover(pygame.key.get_pressed())
        #self.cmundo.actualiza()
        self.mmundo.actualizar()
        self.vmundo.dibujar(pantalla)


class Mundo:
    def __init__(self, mapa, dimension, pantalla):
        mmundo = MMundo(mapa, dimension, pantalla)
        vmundo = VMundo(mmundo)
        self.cmundo = CMundo(mmundo, vmundo)

    def actualiza(self, pantalla):
        self.cmundo.actualiza(pantalla)