# -*- coding: utf-8 -*-
from modelo import *
import pygame
from Observador import *
from Punto import *
from pygame.locals import *


class BotonImagen(Imagen, Observador):
    def __init__(self, nombre, x, y):
        rect = pygame.Rect(x, y, 50, 50)
        Imagen.__init__(self, nombre, rect, Punto(0, 0), False)

    def dibujar(self, pantalla):

        self.dibujo(pantalla, False)
