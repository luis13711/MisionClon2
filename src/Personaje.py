# -*- coding: utf-8 -*-
from Colision import *

from modelo import *

from ColisionCasa import *
from ColisionVentana import *


class Personaje(Imagen):
    def __init__(self, mapa, j=None, i=None):
        if j is not None and i is not None:
            #ubicación del personaje
            rect = pygame.Rect(j * 50, i * 50, 50, 50)
            Imagen.__init__(self, mapa.nvi, rect, mapa.punto, False)
        else:
            #ubicación del personaje
            lfila = 0
            for fila in mapa.matriz:
                lcolum = 0
                for columna in fila:
                    if columna == 1:
                        rect = pygame.Rect(lcolum * 50, lfila * 50, 50, 50)
                        Imagen.__init__(self, mapa.nju, rect, mapa.punto, True)
                    lcolum += 1
                lfila += 1
        #mapa del mundo
        self.mapa = mapa
        #dimensiones del mundo
        self.ventana = mapa.ventana

        #colisionventana
        self.coli_ventana = ColisionVentana(self.ventana, self)
        #vida del personaje
        self.vida = 100
        #puntuación
        self.puntuacion = 0

        comidas = Comida(self.mapa)
        #colisiones con comida
        self.colisioncomida = Colisiones(comidas)
        #casas del mundo
        casas = Casa(self.mapa)
        #colisiones con objetos del mundo
        self.colisionescasa = ColisionCasa(casas)

    def cambiar_imagen2(self):
        Imagen.cambiar_imagen(self.mapa.nvi, True)

    def derecha(self):
        #derecha
        self.rect.x += 10
        self.coli_ventana.derecha()
        self.colisionescasa.derecha(self)
        self.colisioncomida.actualizar(self)

    def izquierda(self):
        #izquierda
        self.rect.x -= 10
        self.coli_ventana.izquierda()
        self.colisionescasa.izquierda(self)
        self.colisioncomida.actualizar(self)

    def arriba(self):
        #arriba
        self.rect.y -= 10
        if self.rect.y < self.ventana.y:
            self.rect.y = self.ventana.y
        self.colisionescasa.arriba(self)
        self.colisioncomida.actualizar(self)

    def abajo(self):
        #abajo
        self.rect.y += 10
        self.coli_ventana.abajo()
        self.colisionescasa.abajo(self)
        self.colisioncomida.actualizar(self)

    def colision(self):
        self.colisioncomida.actualizar(self)

    def dibujar(self, pantalla):
        self.dibujo(pantalla, False)


class PersonajeMovimiento(Imagen):
    def __init__(self, mapa):
        rect = pygame.Rect(50, 50, 50, 50)
        Imagen.__init__(self, mapa.nvi, rect, mapa.punto, False)
        #mapa del mundo
        self.mapa = mapa
        #dimensiones del mundo
        self.ventana = mapa.ventana
        #colisionventana
        self.coli_ventana = ColisionVentana(self.ventana, self)

    def derecha(self):
        #derecha
        self.rect.x += 10
        self.coli_ventana.derecha()

    def izquierda(self):
        #izquierda
        self.rect.x -= 10
        self.coli_ventana.izquierda()

    def arriba(self):
        #arriba
        self.rect.y -= 10
        if self.rect.y < self.ventana.y:
            self.rect.y = self.ventana.y

    def abajo(self):
        #abajo
        self.rect.y += 10
        self.coli_ventana.abajo()


