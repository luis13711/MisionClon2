# -*- coding: utf-8 -*-
from pygame.locals import *
import pygame


blanco = (255, 255, 255)
rojo = (255, 0, 0)
azul = (0, 0, 255)
negro = (0, 0, 0)


class PanelInicio:
    def __init__(self, mapa, dimension, pantalla):
        ANCHO = dimension[0]
        self.pantalla = pantalla
        self.mapa = mapa
        self.btn1 = Boton(ANCHO / 3, 50, 200, 100, "Inicio", azul)
        self.btn2 = Boton(ANCHO / 3, 160, 200, 100, "Crear mapa", azul)
        self.btn3 = Boton(ANCHO / 3, 270, 200, 100, "Salir", azul)
        #id del panel
        self.id = 1

    def getId(self):
        return self.id

    def dibujar(self):
        self.btn1.dibujar(self.pantalla, self.punto)
        self.btn2.dibujar(self.pantalla, self.punto)
        self.btn3.dibujar(self.pantalla, self.punto)

    def establecerPunto(self, punto):
        self.punto = punto

    def evento(self, punto):
        rectangulo = pygame.Rect(punto[0], punto[1], 5, 5)
        rect = self.btn1.rect
        if rectangulo.colliderect(rect):
            return 1
        rect = self.btn2.rect
        if rectangulo.colliderect(rect):
            return 2
        rect = self.btn3.rect
        if rectangulo.colliderect(rect):
            return 3


class Boton:
    def __init__(self, x, y, ANCHO, ALTO, titulo, color):
        self.rect = pygame.Rect(x, y, ANCHO, ALTO)
        self.titulo = titulo
        self.color = color
        self.color_pulsado = (255, 255, 255)

    def dibujar(self, pantalla, punto):
        rectangulo = pygame.Rect(punto[0], punto[1], 5, 5)
        rect = rectangulo.colliderect(self.rect)
        color_cuadro = (self.color, self.color_pulsado)[rect]
        rect = rectangulo.colliderect(self.rect)
        color_texto = (blanco, negro)[rect]

        fuente = pygame.font.Font(None, 20)
        pygame.draw.rect(pantalla, color_cuadro, self.rect, 0)
        mensaje = fuente.render(self.titulo, 1, color_texto)
        x = self.rect.x + self.rect.width / 3
        y = self.rect.y + self.rect.height / 3
        pantalla.blit(mensaje, (x, y))